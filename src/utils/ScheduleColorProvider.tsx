import { ScheduleColors } from "@/models";

const scheduleColors = ["#C2F0FF", "#BFFFDC", "#C7CDFF"];

export const scheduleColorProvider = (index: number) =>
  scheduleColors[index % scheduleColors.length];
