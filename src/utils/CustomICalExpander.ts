import { ScheduleEvent } from "@/models";
import IcalExpander from "ical-expander";

export class CustomICalExpander {

  private expander: IcalExpander;
  private daysCache: Map<number, ScheduleEvent[]> = new Map();
  private allEvents: ScheduleEvent[];

  constructor(icalendarContent: string) {
    this.expander = new IcalExpander({ maxIterations: 1000, ics: icalendarContent });
    this.allEvents = this.all();
  }
  public getTitle(): string {
    return this.expander.component.getFirstPropertyValue("x-wr-calname") || "";
  }

  public getEventsForDay(date: Date): ScheduleEvent[] {
    const copy = new Date(date);
    copy.setHours(0, 0, 0, 0);
    const dayStart = new Date(copy);

    const cached = this.daysCache.get(dayStart.getTime())
    if (cached) {
      return cached;
    }

    copy.setDate(copy.getDate() + 1);
    const dayEnd = new Date(copy);
    const result = this.allEvents.filter(e => e.start.getTime() >= dayStart.getTime() && e.end.getTime() <= dayEnd.getTime());
    this.daysCache.set(dayStart.getTime(), result);
    return result;
  }

  public all(): ScheduleEvent[] {

    const events: ScheduleEvent[] = [];
    const result = this.expander.between();
    for (const occ of result.occurrences) {
      events.push({
        id: occ.item.uid,
        start: occ.startDate.toJSDate(),
        end: occ.endDate.toJSDate(),
        title: occ.item.summary,
        description: occ.item.description,
        type: occ.item.component.getFirstPropertyValue("categories") || "",
        location : occ.item.location || "",
        online_link: occ.item.component.getFirstPropertyValue("x-online-event-link") || ""
      });
    }
    for (const ev of result.events) {
      events.push({
        id: ev.uid,
        start: ev.startDate.toJSDate(),
        end: ev.endDate.toJSDate(),
        title: ev.summary,
        description: ev.description,
        type: ev.component.getFirstPropertyValue("categories") || "",
        location: ev.location || "",
        online_link: ev.component.getFirstPropertyValue("x-online-event-link") || ""
      });
    }

    return events;
  }

}