declare module 'ical-expander' {
  export interface IcalExpanderOptions {
    ics: string;
    maxIterations: number;
  }

  export interface ICalTime {
    get year(): number;
    get month(): number;
    get day(): number;
    get hour(): number;
    get minute(): number;
    get second(): number;
    toJSDate(): Date;
  }

  export interface ICalComponent {
    getFirstPropertyValue(name: string): string?;
  }

  export interface ICalEvent {
    get uid(): string;
    get startDate(): ICalTime;
    get endDate(): ICalTime;
    get summary(): string;
    get description(): string;
    get location(): string;
    get component(): ICalComponent;
  }

  export interface ICalOccurrence {
    item: ICalEvent;
    startDate: ICalTime;
    endDate: ICalTime;
  }

  export interface BetweenResult {
    events: ICalEvent[];
    occurrences: ICalOccurrence[];
  }


  export default class IcalExpander {
    constructor(options: IcalExpanderOptions);

    get component(): ICalComponent;
    public between(from?: Date, to?: Date): BetweenResult;
  }

}