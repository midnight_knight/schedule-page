export class Configuration {
  public static get ScheduleBaseAddress(): string {
    return process.env.NEXT_PUBLIC_SCHEDULE_BASE_ADDRESS || "";
  }
}