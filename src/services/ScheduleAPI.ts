import { Configuration } from "@/utils/Configuration"

export const schedulesSearch: (match: string) => Promise<ListResponse<ScheduleEntry>> = async (match) => {
  var result = await fetch(`${Configuration.ScheduleBaseAddress}/schedule/api/search?limit=15&match=${encodeURIComponent(match)}`);
  return await result.json();

}

// TODO: изменить на нормальный кеш с протуханием результата
const scheduleCache = new Map<string, string>();

export const loadSchedule: (type: ScheduleType, id: number) => Promise<{ content: string, schedule: ScheduleEntry } | null>
  = async (type: ScheduleType, id: number) => {

    const scheduleInfoResponse = await fetch(`${Configuration.ScheduleBaseAddress}/schedule/api/baseinfo?id=${id}&type=${type}`);
    if (scheduleInfoResponse.status !== 200) {
      console.error("server can't return schedule base info", scheduleInfoResponse.status)
      return null;
    }
    const schedule = await scheduleInfoResponse.json() as ScheduleEntry;
    const link = schedule.iCalLink;

    // Убран кеш, так как мешает бесконечная его жизнь
    // const cached = scheduleCache.get(link);
    // if (cached) {
    //   return { content: cached, schedule };
    // }

    const icsResponse = await fetch(link);
    if (icsResponse.status !== 200) {
      console.error("server can't return ical", icsResponse.status)
      return null;
    }
    const content = await icsResponse.text();
    scheduleCache.set(link, content);
    return { content, schedule }
  };


export interface ListResponse<T> {
  data: T[]
}

export enum ScheduleType {
  Unknown = 0,
  Group = 1,
  Teacher = 2,
  Auditorium = 3
}

export interface ScheduleEntry {
  id: number
  targetTitle: string
  fullTitle: string
  scheduleTarget: ScheduleType
  iCalLink: string
  scheduleImageLink: string
  scheduleUpdateImageLink: string
}
