import { Schedule, ScheduleColors } from "@/models";
import React from "react";
import { Calendar } from "rsuite";
import { BadgeCalendar } from "./BadgeCalendar";
import { scheduleColorProvider } from "@/utils/ScheduleColorProvider";

type CompactCalendarProps = {
  schedules: Schedule[];
  currentDate: Date;
  onSelectDateEvent: (newValue: Date) => void;
};

export const CompactCalendar = ({
  schedules,
  currentDate,
  onSelectDateEvent,
}: CompactCalendarProps) => {
  const renderCell = (date: Date) => {
    if (schedules.length == 0) {
      return;
    }

    return schedules
      .filter((s) => s.calendar)
      .map((e, i) => {
        const eventsCount = new Set(e.calendar!.getEventsForDay(date).map(e => e.start.getTime())).size;
        if (eventsCount !== 0) {
          return (
            <BadgeCalendar
              key={`${e.id}_${e.title}_${i}`}
              disabled={currentDate.getMonth() != date.getMonth()}
              colorBadge={scheduleColorProvider(i)}
              content={eventsCount}
            />
          );
        }
      });
  };

  return (
    <Calendar
      compact
      bordered
      isoWeek
      renderCell={renderCell}
      value={currentDate}
      onSelect={(e: Date) => onSelectDateEvent(e)}
      onChange={(e: Date) => onSelectDateEvent(e)}
    />
  );
};
export default CompactCalendar;
