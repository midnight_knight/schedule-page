import { FlexboxGrid } from "rsuite";
import styles from "./CurrentDay.module.css";

type CurrentDateProps = {
  date: Date;
  size: "lg" | "md";
};

const stylesForSize = {
  date: {
    lg: styles.date_lg,
    md: styles.date_md,
  },
};

export const getWeekNumber = (date: Date) => {
  const startDate = new Date("2023-09-01");
  const dayMilliseconds = 24 * 60 * 60 * 1000;
  const asd = date.getDay() - 1;
  const zxc = asd < 0 ? 6 : asd;

  const cloneCurrentDate = new Date(date);
  const start_week = new Date(
    cloneCurrentDate.setTime(cloneCurrentDate.getTime() - zxc * dayMilliseconds)
  );
  const diffInTime = start_week.getTime() - startDate.getTime();
  const diffInDays = Math.round(diffInTime / (60 * 60 * 24 * 1000));
  const count_week = Math.ceil(diffInDays / 7) + 1;
  return count_week;
};

export const CurrentDate = ({ date, size }: CurrentDateProps) => {
  const weekNum = getWeekNumber(date);
  const showWeekNum = weekNum > 0 && weekNum <= 18;
  return (
    <FlexboxGrid align="middle" justify="center">
      <FlexboxGrid.Item>
        <p className={stylesForSize.date[size]}>
          {date.toLocaleDateString("ru-RU")}
        </p>
      </FlexboxGrid.Item>
      {showWeekNum && (
        <FlexboxGrid.Item>
          <p className={`${styles.week} ${size}`}>Неделя №{weekNum}</p>
        </FlexboxGrid.Item>
      )}
    </FlexboxGrid>
  );
};
