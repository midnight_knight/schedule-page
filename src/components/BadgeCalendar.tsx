import React from "react";
import { Badge } from "rsuite";

type BadgeCalendarProps = {
  colorBadge: string;
  disabled: boolean;
  content?: number;
};

export const BadgeCalendar = ({
  colorBadge,
  content,
  disabled,
}: BadgeCalendarProps) => {
  return (
    <Badge
      style={{
        background: colorBadge,
        color: disabled ? "var(--rs-text-disabled)" : "var(--rs-text-primary)",
      }}
      content={content}
    />
  );
};
