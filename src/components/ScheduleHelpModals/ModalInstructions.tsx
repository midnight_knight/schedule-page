import React, { useEffect, useState } from "react";
import {
  Button,
  IconButton,
  Divider,
  Container,
  Header,
  Content,
  Modal,
  Nav,
  Message,
  Tooltip,
  Whisper,
} from "rsuite";

import Image from "next/image";
import styles from "./ModalInstructions.module.css";
import { Android, Calendar, IOs, Others } from "@rsuite/icons";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import useDomLoaded from "@/utils/UseDomLoaded";

type TabType = "Android" | "iOS" | "another";

type ModalInstructionsProps = {
  scheduleId: string;
  href: string;
  scheduleTitle: string;
};
const IOS_INSTRUCTION_STEPS = [
  {
    step: 1,
    text: "После нажатия на кнопку добавления календаря, автоматически осуществится переход на добавление подписного календаря. Нажмите на кнопку «Подписаться».",
    imageSrc: "/assets/iosStep2.jpg",
    width: 591,
    height: 1231,
  },
  {
    step: 2,
    text: "Проверьте название календаря и при необходимости просмотрите события внутри него. Нажмите на кнопку «Добавить».",
    imageSrc: "/assets/iosStep3.jpg",
    width: 590,
    height: 1231,
  },
  {
    step: 3,
    text: "Убедитесь, что галочка стоит напротив нужного названия календаря. Нажмите на кнопку «Готово».",
    imageSrc: "/assets/iosStep4.jpg",
    width: 590,
    height: 1240,
  },
  {
    step: 4,
    text: "При просмотре календаря по дням, события появятся в следующем виде.",
    imageSrc: "/assets/iosStep5.jpg",
    width: 592,
    height: 1229,
  },
];

const ANDROID_INSTRUCTION_STEPS = [
  {
    step: 1,
    text: "Нажмите на кнопку добавления календаря. Войдите в аккаунт Google. Нажмите на кнопку «Добавить».",
    imageSrc: "/assets/androidStep2.jpg",
    width: 970,
    height: 1805,
  },
  {
    step: 2,
    text: "Установите приложение Google Calendar. Откройте его. Войдите в свою учетную запись.",
    imageSrc: "/assets/androidStep3.jpg",
    width: 523,
    height: 1168,
  },
  {
    step: 3,
    text: "Календарь автоматически появится на главном экране.",
    imageSrc: "/assets/androidStep4.jpg",
    width: 524,
    height: 1164,
  },
];

const ANDROID_INSTRUCTION_STEPS_ADDITION = [
  {
    step: 1,
    text: "Перейдите в главное меню.",
    imageSrc: "/assets/androidStepAddition1.jpg",
    width: 524,
    height: 1167,
  },
  {
    step: 2,
    text: "Найдите нужный календарь в списке. При наличии большого списка календарей нужно развернуть список кнопкой «Еще».",
    imageSrc: "/assets/androidStepAddition2.jpg",
    width: 524,
    height: 1166,
  },
  {
    step: 3,
    text: "Убедитесь в том, что галочка стоит.",
    imageSrc: "/assets/androidStepAddition3.jpg",
    width: 524,
    height: 1169,
  },
  {
    step: 4,
    text: "Если галочка стоит, а события не появились, необходимо перейти в настройки.",
    imageSrc: "/assets/androidStepAddition4.jpg",
    width: 523,
    height: 1164,
  },
  {
    step: 5,
    text: "В списке необходимо нажать на нужный календарь.При наличии большого списка календарей нужно развернуть список кнопкой «Еще».",
    imageSrc: "/assets/androidStepAddition5.jpg",
    width: 524,
    height: 1176,
  },
  {
    step: 6,
    text: "Активировать переключатель «Синхронизация». Перезапустить приложение.",
    imageSrc: "/assets/androidStepAddition6.jpg",
    width: 523,
    height: 1171,
  },
];

const renderStep = (arrayStep: any[]) => {
  return arrayStep.map((e, i) => (
    <div key={i}>
      <Divider className={styles.tag_popover_dropdown_modal_header}>
        Шаг {e.step}
      </Divider>
      <div className={styles.tag_popover_dropdown_modal_body}>
        <Image
          src={e.imageSrc}
          alt={e.text}
          width={e.width}
          height={e.height}
          sizes="100vw"
          style={{ width: "50%", height: "auto" }}
        />
        <div style={{ paddingLeft: "20px" }}>{e.text}</div>
      </div>
    </div>
  ));
};

const renderContentAndroid = () => {
  return (
    <>
      {renderStep(ANDROID_INSTRUCTION_STEPS)}
      <div
        className={styles.tag_popover_dropdown_modal_body}
        style={{ fontSize: "18px", marginTop: "30px" }}
      >
        Что делать, если события в нужном календаре не появились?
      </div>
      {renderStep(ANDROID_INSTRUCTION_STEPS_ADDITION)}
    </>
  );
};

const renderContentIOS = () => {
  return <>{renderStep(IOS_INSTRUCTION_STEPS)}</>;
};

const contentForAddButton = (typeOS: TabType) => {
  return typeOS === "Android"
    ? "Добавить для Android"
    : typeOS === "iOS"
    ? "Добавить для iOS"
    : "Добавить";
};

const searchKey = "addToCal";

const ModalInstructions = ({
  href,
  scheduleTitle,
  scheduleId,
}: ModalInstructionsProps) => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace } = useRouter();
  const [typeOS, setTypeOS] = useState<TabType>("Android");
  const domLoaded = useDomLoaded();
  if (!domLoaded) {
    // пропуск рендера на сервере, так как модальное окно отрисованное на сервере приводит к ошибке гидратации
    return null;
  }

  const open = searchParams.get(searchKey)?.toString() === scheduleId;

  const setOpen = (isOpen: boolean) => {
    const params = new URLSearchParams(searchParams);
    if (isOpen) {
      params.set(searchKey, scheduleId);
    } else {
      params.delete(searchKey);
    }
    replace(`${pathname}?${params.toString()}`);
  };

  const addCalendarLink = href.replace("https", "webcal");

  const renderContent = (typeOS: TabType) => {
    return (
      <div>
        {typeOS === "Android" ? (
          renderContentAndroid()
        ) : typeOS === "iOS" ? (
          renderContentIOS()
        ) : (
          <>
            <p>
              Многие сервисы поддерживают добавление онлайн календарей,
              &quot;календарей из сети&quot; по ссылке. Вы можете самостоятельно
              добавить такой календарь используя следующую ссылку:
            </p>
            <Message>
              <p style={{ userSelect: "all" }}>{addCalendarLink}</p>
            </Message>
            <Divider />
            <p>Или перейти по ней при помощи кнопки ниже</p>
          </>
        )}
      </div>
    );
  };

  const linkForButton = (typeOS: string) => {
    return typeOS == "Android"
      ? `https://www.google.com/calendar/render?cid=${addCalendarLink}`
      : addCalendarLink;
  };
  return (
    <>
      <Whisper
        trigger="hover"
        placement={"auto"}
        speaker={<Tooltip>Добавить в календарь</Tooltip>}
      >
        <IconButton
          onClick={() => setOpen(true)}
          icon={<Calendar />}
        ></IconButton>
      </Whisper>
      <Modal open={open} onClose={() => setOpen(false)}>
        <Modal.Body>
          <Container>
            <Header>
              <h4>Добавление {scheduleTitle} в личный календарь</h4>
              <Nav
                appearance="subtle"
                activeKey={typeOS}
                style={{ width: "100%" }}
                onSelect={(v) => setTypeOS(v as TabType)}
              >
                <Nav.Item
                  eventKey="Android"
                  icon={<Android />}
                  style={{ width: "33%", justifyContent: "center" }}
                >
                  Android
                </Nav.Item>
                <Nav.Item
                  eventKey="iOS"
                  icon={<IOs />}
                  style={{ width: "33%", justifyContent: "center" }}
                >
                  iOS
                </Nav.Item>
                <Nav.Item
                  eventKey="another"
                  icon={<Others />}
                  style={{ width: "33%", justifyContent: "center" }}
                >
                  ПК/Прочее
                </Nav.Item>
              </Nav>
            </Header>
            <Content style={{ padding: "20px" }}>
              {renderContent(typeOS)}
            </Content>
          </Container>
        </Modal.Body>
        <Modal.Footer className={styles.footer_container}>
          <Button
            href={linkForButton(typeOS)}
            target="_blank"
            className="button_desk"
            style={{ width: "100%" }}
          >
            {contentForAddButton(typeOS)}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalInstructions;
