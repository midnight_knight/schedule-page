import React, { useState } from "react";
import { Button, IconButton, Loader, Modal, Radio, RadioGroup, Tooltip, Whisper } from "rsuite";
import { Schedule } from "@/models";
import { Gallery, Item } from "react-photoswipe-gallery";
import "photoswipe/dist/photoswipe.css";
import { TextImage } from "@rsuite/icons";

interface ModalMoreInformationProps {
  schedule: Schedule;
}

const ModalMoreInformation = ({ schedule }: ModalMoreInformationProps) => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [imgLink, setImgLink] = useState<string>(schedule.scheduleImageLink);

  const [imgSize, setImageSize] = useState<{ width: number; height: number }>({
    width: 0,
    height: 0,
  });

  const [isLoading, setIsLoading] = useState(true);

  const imageChanged = (src: string) => {
    setImgLink(src);
    setIsLoading(true);
  };

  const imageCreated = (img: HTMLImageElement | null) => {
    if (!img) {
      return;
    }
    // после того, как изображение загрузилось - обновляем его размеры
    const loadHandler = () => {
      const newValues = { width: img.width, height: img.height };
      if (
        (newValues.width != imgSize.width ||
          newValues.height != imgSize.height) && // только если они загрузились
        newValues.width > 0
      ) {
        setImageSize(newValues);
      }
      setIsLoading(false);
      img.removeEventListener("load", loadHandler);
    };
    img.addEventListener("load", loadHandler);
  };
  const imageLoader = () => {
    return imgLink;
  };
  return (
    <>
      <Whisper
        trigger="hover"
        placement={"auto"}
        speaker={<Tooltip>Расписание целиком</Tooltip>}
      >
        <IconButton icon={<TextImage />} onClick={() => handleOpen()} />
      </Whisper>
      <Modal size="sm" open={open} onClose={handleClose}>
        <Modal.Header>
          <RadioGroup
            inline
            appearance="picker"
            value={imgLink}
            style={{ width: "100%", justifyContent: "center" }}
            onChange={(v) => imageChanged(v as string)}
          >
            <Radio
              value={schedule.scheduleImageLink}
              style={{ width: "50%", textAlign: "center" }}
            >
              Расписание
            </Radio>
            <Radio
              value={schedule.scheduleUpdateImageLink}
              style={{ width: "50%", textAlign: "center" }}
            >
              Обновление
            </Radio>
          </RadioGroup>
        </Modal.Header>
        <Modal.Body>
          {isLoading && (
            <Loader
              size="md"
              style={{
                position: "absolute",
                marginLeft: "auto",
                marginRight: "auto",
                left: 0,
                right: 0,
                top: "8px",
                textAlign: "center",
              }}
            />
          )}
          <Gallery
            options={{
              initialZoomLevel: 2,
              secondaryZoomLevel: 3,
              maxWidthToAnimate: 4,
            }}
          >
            <Item
              original={imageLoader()}
              thumbnail={imageLoader()}
              width={imgSize.width}
              height={imgSize.height}
            >
              {({ ref, open }) => (
                <img
                  ref={(r) => {
                    ref.current = r!;
                    imageCreated(r);
                  }}
                  onClick={open}
                  src={imageLoader()}
                  style={{
                    maxWidth: "100%",
                    cursor: "pointer",
                  }}
                />
              )}
            </Item>
          </Gallery>
        </Modal.Body>
        <Modal.Footer>
          <a href={imageLoader()} download={`${schedule.title}.pnp`}>
            <Button onClick={handleClose} appearance="primary" block>
              Сохранить
            </Button>
          </a>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalMoreInformation;
