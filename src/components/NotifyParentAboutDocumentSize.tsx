"use client";

import { useEffect } from "react";

function NotifyParentAboutDocumentSize() {
  useEffect(() => {
    const sizeSender = () => {
      window.parent.postMessage(
        {
          messageType: "resize",
          size: {
            width: document.body.scrollWidth,
            height: document.body.scrollHeight,
          },
        },
        "*"
      );
    };

    var resizeObserver = new ResizeObserver(() => {
      sizeSender();
    });
    resizeObserver.observe(window.document.body);

    const messageEventListener = (message: MessageEvent<any>) => {
      if (message.data.messageType == "get_initial_size") {
        sizeSender();
      }
    };
    window.addEventListener("message", messageEventListener);

    return () => {
      resizeObserver.unobserve(window.document.body);
      resizeObserver.disconnect();
      window.removeEventListener("message", messageEventListener);
    };
  }, []);

  return <></>;
}

export default function Wrapper() {
  if (typeof window === "undefined") {
    return null;
  }

  return <NotifyParentAboutDocumentSize />;
}
