import { AutoComplete } from "rsuite";
import { useState } from "react";
import useSWR from "swr";
import {
  ListResponse,
  ScheduleEntry,
  schedulesSearch,
} from "@/services/ScheduleAPI";
import { Schedule, SchedulePointer, ScheduleRow } from "@/models";

import {
  parseQuerySchedule,
  serializeQuerySchedule,
} from "@/utils/ScheduleInSearchUtils";
import { ScheduleLabel } from "./ScheduleLabel";

type ScheduleSelectorProps = {
  disabled: boolean;
  schedules: Schedule[];
  onScheduleSelected: (schedule: SchedulePointer) => void;
};

const ScheduleSelector = ({
  disabled,
  schedules: alreadySelectedSchedules,
  onScheduleSelected,
}: ScheduleSelectorProps) => {
  const [match, setMatch] = useState<string>("");

  const { data: schedules, isLoading } = useSWR<ListResponse<ScheduleEntry>>(
    [match, "searchSchedule"],
    (args: string[]) => schedulesSearch(args[0])
  );

  // AutoComplete работает только с текстом, потому ключ и ввод прилетают по одному пут
  const textChanged = (text: string) => {
    const pointer = parseQuerySchedule(text);
    if (pointer) {
      onScheduleSelected(pointer);
      setMatch("");
      return;
    }
    setMatch(text);
  };

  return (
    <>
      <AutoComplete
        disabled={disabled}
        placeholder={
          "Добавить"
        }
        placement="auto"
        filterBy={() => !!match}
        data={
          schedules?.data
            .filter(
              (s) =>
                !alreadySelectedSchedules.find(
                  (a) => a.id == s.id && a.scheduleTarget == s.scheduleTarget
                )
            )
            .map((s) => ({
              label: s.fullTitle,
              value: serializeQuerySchedule(s),
              source: s,
            })) || []
        }
        renderMenuItem={(_, item) => {
          return <ScheduleLabel schedule={item.source} />;
        }}
        value={match}
        onChange={textChanged}
      />
    </>
  );
};

export default ScheduleSelector;
