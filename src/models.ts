import { TypeAttributes } from "rsuite/esm/@types/common"
import { CustomICalExpander } from "./utils/CustomICalExpander"
import { ScheduleType } from "./services/ScheduleAPI"

export type ScheduleEvent = {
  id: string,
  start: Date,
  end: Date,
  type: string,
  title: string,
  description: string,
  location: string,
  online_link: string,
}

const hashCode = (value: string) => {
  var hash = 0,
    i, chr;
  if (value.length === 0) return hash;
  for (i = 0; i < value.length; i++) {
    chr = value.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash = hash & hash;
  }
  return hash;
}

export class ScheduleColors {
  constructor(public primaryColor: TypeAttributes.Color, public primaryColorHex: string, private variants: string[]) {
  }

  public getVariantForEventType(type: string): string {
    if (this.variants.length < 2) {
      console.warn("Need 2 colors for correct view");
      return this.primaryColorHex;
    }
    const number_color = hashCode(type) % this.variants.length
    return this.variants[number_color]
  }
}
/** Указатель на расписание, исключительно информация про его идентификатор */
export type SchedulePointer = {
  scheduleTarget: ScheduleType;
  id: number;
}

/** То, что необходимо для представления строки в UI */
export type ScheduleRow = SchedulePointer & {
  /**
   * Расписание закреплено в UI статично и пользователь не может его удалить
   */
  isStatic: boolean;
}
/** Загруженное расписание с наполнением и контентом. Используется для передачи с server props */
export type ScheduleTarget = ScheduleRow & {
  title: string;
  iCalLink: string;
  iCalContent: string;
  scheduleImageLink: string;
  scheduleUpdateImageLink: string;
};
/** Расписание с разобранным наполнением для отображения в UI */
export type Schedule = ScheduleTarget & {
  title: string | null;
  calendar?: CustomICalExpander | null;
  icalLink: string;
}
