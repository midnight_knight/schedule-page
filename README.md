# Страница расписания РТУ МИРЭА

## Конфигурация
### Локальная разработка
Создать файл `.env.local`, со следующим контентом:
```
NEXT_PUBLIC_SCHEDULE_BASE_ADDRESS=базовый путь к API расписания
SENTRY_DSN=DSN от Sentry, на проде, или при необходимости во время локальной разработки
NEXT_PUBLIC_SENTRY_DSN=DSN от Sentry, на проде, или при необходимости во время локальной разработки
```
## Разработка

### Запустить локально
```bash
npm run dev
```
