const ContentSecurityPolicy = `
  img-src *;
  default-src 'self';
  script-src 'self' 'unsafe-eval';
  style-src 'self' 'unsafe-inline';
  font-src 'self' data:;
  connect-src 'self' ${process.env.NEXT_PUBLIC_SCHEDULE_BASE_ADDRESS || ''};
  worker-src 'self' blob:;
  child-src 'self' blob:;
  object-src 'self' data:;  
`;

const rewriteBeforeFiles = [];
if (process.env.NEXT_PUBLIC_SENTRY_DSN) {
  const { origin, pathname } = new URL(process.env.NEXT_PUBLIC_SENTRY_DSN);
  rewriteBeforeFiles.push({
    source: `/monitoring(/?)`,
    destination: `${origin}/api${pathname}/envelope/`,
  });
}

/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    buildDate: new Intl.DateTimeFormat('ru-RU', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour12: false,
      hour: '2-digit',
      minute: '2-digit'
    }).format(new Date())
  },
  reactStrictMode: true,
  output: 'standalone',
  async headers() {
    return [
      {
        source: "/:all*",
        headers: [
          {
            key: 'Content-Security-Policy',
            value: ContentSecurityPolicy.replace(/\s{2,}/g, ' ').trim()
          }
        ]
      }
    ]
  },
  async rewrites() {
    return {
      beforeFiles: rewriteBeforeFiles
    }
  }
}

module.exports = nextConfig


// Injected content via Sentry wizard below

const { withSentryConfig } = require("@sentry/nextjs");

module.exports = withSentryConfig(
  module.exports,
  {
    // For all available options, see:
    // https://github.com/getsentry/sentry-webpack-plugin#options

    silent: false,

    org: "rtu-schedule",
    project: "rtu-schedule-page",
  },
  {
    // For all available options, see:
    // https://docs.sentry.io/platforms/javascript/guides/nextjs/manual-setup/

    // Upload a larger set of source maps for prettier stack traces (increases build time)
    widenClientFileUpload: true,

    // Transpiles SDK to be compatible with IE11 (increases bundle size)
    transpileClientSDK: true,

    // Hides source maps from generated client bundles
    hideSourceMaps: true,

    // Automatically tree-shake Sentry logger statements to reduce bundle size
    disableLogger: true,
  }
);
